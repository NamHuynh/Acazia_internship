var slide_index = 1;
var time;
var obj_img = {
    "img-slide": ['https://photo-zmp3.zadn.vn/banner/2/a/3/5/2a35dbf152f5c9eb571e789985982b04.jpg',
        'https://photo-zmp3.zadn.vn/banner/f/0/7/4/f0748f6f9c90482b26e65a78b33a8fc7.jpg',
        'https://photo-zmp3.zadn.vn/banner/6/4/5/2/6452cacb601ba8b4c944668840c437e4.jpg',
        'https://photo-zmp3.zadn.vn/banner/a/8/8/0/a880ebf01d1200281a24b7477913de2a.jpg'
    ],
    "img-thumbnail": ['https://photo-resize-zmp3.zadn.vn/w94h94_jpeg/avatars/a/d/3/b/ad3b4070de0335c6f1601815a23ef7f0.jpg',
        'https://photo-resize-zmp3.zadn.vn/w94h94_jpeg/avatars/b/b/f/f/bbffe373dde09afe634f10bcd3e65d7d.jpg',
        'https://photo-resize-zmp3.zadn.vn/w94h94_jpeg/avatars/c/0/f/3/c0f33c43c5ecd0f0171f873ffbcd837f.jpg',
        'https://photo-resize-zmp3.zadn.vn/w94h94_jpeg/avatars/0/f/d/d/0fdd38ff0ef0f4de4f1d80e8304c717e.jpg'
    ]
}
function set_link_img(){
    var text1 = "";
    var text2 = "";
    for (var i = 0; i < obj_img["img-slide"].length; i++) {
        text1 += '<div class="slide-item" id="slide-item">'
            + " <img src=" + obj_img['img-slide'][i] + ">"
            + '</div>';
    }
    document.getElementsByClassName("slide")[0].innerHTML = text1;
    for (var i = 0; i < obj_img["img-thumbnail"].length; i++) {
        text2 += '<span onmousemove="" >'
            + " <img class='thumbnail' src="+obj_img["img-thumbnail"][i]+" alt=''>"
            + '</span>';
    }
    document.getElementsByClassName("slide-thumbnail")[0].innerHTML=text2;
}

function show_slide(slide_index) {
    clearTimeout(time);
    var total_thumbnail = document.getElementsByClassName("thumbnail").length;
    var img_location=(slide_index - 1)*1024;
    document.getElementsByClassName("slide")[0].setAttribute("style","transform: translateX(-"+img_location+"px);");
    for (var i = 0; i < total_thumbnail; i++) {
        document.getElementsByClassName("thumbnail")[i].classList.remove("active");
    }
    document.getElementsByClassName("thumbnail")[slide_index - 1].classList.add("active");
    time=setTimeout(auto_run_slide, 3000);
}
function auto_run_slide() {
    slide_index=slide_index+1;
    var total_slide = document.getElementsByClassName("slide-item").length;
    if (slide_index > total_slide) slide_index = 1;
    if (slide_index < 1) slide_index = total_slide;
    show_slide(slide_index);
}

function set_mouse_move() {
    var total_thumbnail = document.getElementsByClassName("thumbnail").length;
    for (var i = 0; i < total_thumbnail; i++) {
        document.querySelectorAll("span")[i].setAttribute("onmousemove", "curren_slide(" + (i + 1) + ")");
    }
}

function curren_slide(index_image) {
    slide_index = index_image;
    show_slide(index_image);
}

document.getElementsByClassName("control-prev")[0].addEventListener("click", function () {
    slide_index=slide_index-1;
    var total_slide = document.getElementsByClassName("slide-item").length;
    if (slide_index < 1) slide_index = total_slide;
    show_slide(slide_index);
})
document.getElementsByClassName("control-next")[0].addEventListener("click", function () {
    slide_index=slide_index+1;
    var total_slide = document.getElementsByClassName("slide-item").length;
    if (slide_index > total_slide) slide_index = 1;
    show_slide(slide_index);
})

set_link_img();
show_slide(slide_index);
set_mouse_move();





