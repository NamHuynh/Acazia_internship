const GET = "GET";
const POST = "POST";
const PUT = "PUT";
const DELETE = "DELETE";
const AUTHORIZATION = "Authorization";

var makeRequest = function (method, url, data, option) {
  var request = new XMLHttpRequest();

  return new Promise(function (resolve, reject) {

    request.onreadystatechange = function () {

      if (request.readyState !== 4) return;

      if (request.status >= 200 && request.status < 300) {
        resolve(request);
      } else {
        reject({
          status: request.status,
          statusText: request.statusText
        });
      }

    };

    request.open(method, url, true);
    if (localStorage.getItem("token")) {
      request.setRequestHeader(AUTHORIZATION, localStorage.getItem("token"));
    }
    if (option )
      Object.keys(option).map(x => {
        let keys = x;
        let value = option[keys];
        request.setRequestHeader(keys, value);
      })

    if (!(data instanceof FormData)) {
      request.send(JSON.stringify(data));
    } else request.send(data);




  });
};

var makeDownload = function (method, url, data, option) {
  var request = new XMLHttpRequest();

  return new Promise(function (resolve, reject) {

    request.onreadystatechange = function () {

      if (request.readyState !== 4) return;

      if (request.status >= 200 && request.status < 300) {
        resolve(request);
      } else {

        reject({
          status: request.status,
          statusText: request.statusText
        });
      }

    };

    
    request.onload = function () {
   

      var blob = new Blob([this.response]);
      var href = window.URL.createObjectURL(blob);
      var a = document.createElement("a");
      a.href = href;
      a.style.display = "none";
      a.target = "_blank";

      a.download = "filename.xlsx";
      a.click();
      window.URL.revokeObjectURL(href);
    }
    request.open(method, url, true);
    request.responseType = "blob";

    if (localStorage.getItem("token")) {
      request.setRequestHeader(AUTHORIZATION, localStorage.getItem("token"));
    }

    if (option  > 0)
      Object.keys(option).map(x => {
        var keys = x;
        var value = option[keys];
        request.setRequestHeader(keys, value);
      })

    if (!(data instanceof FormData)) {
      request.send(JSON.stringify(data));
    } else request.send(data);




  });
};

var option = {
    "Content-Type": "application/json-patch+json",
    "Accept": "application/json"

}
var get = function (url, option) {
  return makeRequest(GET, url, null, option)
}

var post = function (url, data, option) {
  return makeRequest(POST, url, data, option)
}

var put = function (url, option) {
  return makeRequest(PUT, url, data, option)
}

var del = function (url, option) {
  return makeRequest(DELETE, url, null, option)
}

var download = function (url, option) {
  return makeDownload(GET, url, null, option)
}
// download
document.getElementById("load-xhr").addEventListener("click", function () {
  var option = {
    "Content-Type": "text/csv",
    "Accept": "text/csv"
  }
  var url = "http://ec2-54-169-93-233.ap-southeast-1.compute.amazonaws.com/fablead-server/export_conference?type=xlsx";
  download(url, option)
    .then(result => console.log(result.response))
    .catch(err => console.log(err))
})

// post login
var option ={
  "Content-Type":"application/json-patch+json"
}
var data={
  "userName": "TOAN.HUYNH",
  "password": "a"
}
post("http://115.79.35.119:9008/api/hmk/loginmobile",data,option);

//Post update file
document.getElementById("upload").addEventListener("change", function(e){
    const Data= new FormData;
    const FileImg=e.target.files[0];
    const option = {
        "Content-Type": "multipart/form-data"
    }
    Data.append("File",FileImg);
    post('http://115.79.35.119:9008/api/hmk/image/upload',Data ,option)..then(result => console.log(result.response))
    .catch(err => console.log(err))
})
// Get 
get("http://115.79.35.119:9008/api/hmk/data/all",option)
.then(result => console.log(result.response))
.catch(err => console.log(err))

