//obj server
var data_servers={
    "cpu":{
        "name":"CPU Usage",
        "processes":"348 Processes. ",
        "using":1,
        "total":4,
        "unit":" Cores"
    },
    "memory":{
        "name":"Memory Usage",
        "using":11444,
        "total":16384,
        "unit":"GB"
    },
    "ssd":{
        "name":"SSD 1 Usage",
       "using":243,
       "total":256,
       "unit":"GB"
    },
    
}
// obj notification
var type_icon={
    "new_registered":"fas fa-user-plus text-success",
    "user_delete":"fas fa-user-times text-danger",
    "report":"far fa-chart-bar text-primary",
    "new_client":"fas fa-shopping-basket text-primary",
    "server_overloaded":"fas fa-tachometer-alt text-warning"
}
var notifi=[
    {
        "text_notifi":"New user registered",
        "icon":"new_registered"
    },
    {
        "text_notifi":"User deleted",
        "icon":"user_delete"
    },
    {
        "text_notifi":"Sale report is ready",
        "icon":"report"
    },
    {
        "text_notifi":"New client",
        "icon":"new_client"
    },
    {
        "text_notifi":"Server overloaded",
        "icon":"server_overloaded"
    }
]

// obj messages 
var info_messages=[];
for(var i = 0 ; i < 30 ; i++ ){
    info_messages.push(
        {
            "avatar":"img/6.jpg",
            "sender":"John Doe",
            "time":"6:30:00",
            "messages":"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...",
            "status":"online"
        }
    );
}

// dropdown notification

// notifi
document.getElementById("notifi").innerHTML= notifi.length;
document.getElementById("head-notifi").outerHTML="<h5 class=\"dropdown-header \">You have "+ notifi.length +" notification</h5>";
var i,text="";
for(var i=0;i< notifi.length;i++){
    text+="<a class=\"dropdown-item\"  href=\"#\"><i class=\""+type_icon[notifi[i].icon]+"\"></i>"+notifi[i].text_notifi+"</a>";
}

document.getElementById("drop-item-notifi").outerHTML=text;

// server

function handle_bar(using,total){
    return (using/total)*100;
}

var usage="";

for(var i in data_servers ){
    var small='';
    if (data_servers[i].unit===' Cores'){
        small="<small >"+data_servers[i].processes+data_servers[i].using+"/"+data_servers[i].total+data_servers[i].unit+"."+"</small>"
    }
    else{
        unit=data_servers[i].unit;
        small="<small >"+data_servers[i].using+unit+"/"+data_servers[i].total+data_servers[i].unit+"</small>"
    }
    var color="";
    var bar=handle_bar(data_servers[i].using,data_servers[i].total);
    if (bar<=30){
        color="";
    }
    else if(bar>30 && bar<=70){
        color="bg-warning";
    }
    else {
        color="bg-danger";
    }
    usage+="<a class='dropdown-item ' href='#'> "
            +  "  <b class='text-uppercase'>"+data_servers[i].name+"</b>"
            +   "<div class='progress '  >"  
            +    " <div class='progress-bar "+color+" ' id='cpu-bar' style='width:"+handle_bar(data_servers[i].using,data_servers[i].total)+"%'></div>"   
            +    "</div>"
            +    small
            +"</a> " ;
}

document.getElementById("usage").outerHTML=usage;
                
// dropdown messages

function status(status){
    if(status==="online"){
        return "badge-success";
    }
    if(status==="offline"){
        return "badge-warning";
    }
}

var show_mess=[];


function add_mess(array){
    var length_array;
    length_array=array.length + 5;
    if(length_array>info_messages.length){
        length_array=info_messages.length;
    }
    for(var i=array.length;i<length_array;i++){
        array.push(info_messages[i]);
    }
    list(show_mess);
}


add_mess(show_mess);

document.getElementById("show-list").addEventListener('scroll',function () {
    if(this.scrollTop + this.clientHeight >= this.scrollHeight){
        add_mess(show_mess);      
    }
});

function list(show_mess){
    var list_mess="";
    for(var i=0;i<show_mess.length;i++){ 
        list_mess+= "<a class=\"dropdown-item\" href=\"#\">"
                    +   " <div class=\"messages\">"
                    +       " <div class=\"float-left py-3 mr-3 v\">"
                    +            "<img src=\"" + show_mess[i].avatar + "\" alt=\"avatar\" class=\"img-avatar rounded-circle \">"
                    +            " <span class=\" avatar-status badge "+status(show_mess[i].status)+" rounded-circle\"> </span>"
                    +        "</div>"
                    +        "<div class=\"info\">"
                    +            "<small class=\"float-left\">"+show_mess[i].sender+"</small>"
                    +            "<small class=\"float-right\">"+show_mess[i].time+"</small>"
                    +        "</div>"
                    +        "<div class=\"main-messages text-truncate font-weight-bold\">"
                    +           show_mess[i].messages
                    +        "</div>"
                    +        "<div class=\"extra-messages text-truncate\">"
                    +            "<small >"+show_mess[i].messages+"</small>"
                    +        "</div>"
                    +    "</div>" 
                    + "</a>";
                    
    }
    document.getElementById("show-list").innerHTML=list_mess;
}

document.getElementById("badge-mess").innerHTML=info_messages.length;
document.getElementById("message_notifi").innerHTML="You have "+info_messages.length+" messages ";




