var obj_info_table = [];
for (var i = 0; i < 65; i++) {
    obj_info_table.push(
        {
            "stt": i + 1,
            "province/city": "Tỉnh Bình Phước",
            "District": "Thị xã Phước Long",
            "Commune": "Phường Thác Mơ",
            "Species": "Sao xanh",
            "Volume": "10.20",
            "Area": "0.09",
        }
    );
}
var element_page_size_default= 9;
var element_page_start_default=1;
var total_num_page = Math.ceil(obj_info_table.length / 10);
var current_page_default=1;
var element_default =[];
for (var i=0;i<10;i++){
    element_default[i]=obj_info_table[i];
}
function load_page(current_page) {
    if(current_page>total_num_page) current_page=total_num_page;
    var sub_obj = [];
    var element_page_size = 10;
    var start = (current_page - 1) * element_page_size;
    var element_remainig = obj_info_table.length - start;
    if ((start + element_page_size) > obj_info_table.length) {
        for (var i = 0; i < element_remainig; i++) {
            sub_obj[i] = obj_info_table[start + i];
        }
        show_info_table(sub_obj);
        show_info_page(start, element_remainig ,obj_info_table.length);
    }
    else {
        for (var i = 0; i < element_page_size; i++) {
            sub_obj[i] = obj_info_table[start + i];
        }  
        show_info_table(sub_obj);   
        show_info_page(start,element_page_size,obj_info_table.length); 
    }
    show_info_pagination(total_num_page,current_page);
    active(current_page);
}
function show_info_pagination(total_num_page,current_page) { 
    var page_head=1;
    var page_end=total_num_page; 
    var page_next=current_page+1;
    var page_previous=current_page-1;
    var Limit_hidden_pages=2;
    var Limit_handle_pages=5;
    var page_start=2;
    var limit_current_page_handle=4;
    var text = ""; 
    var next_end = "<a onclick='load_page("+page_end+")'  href=\"#\"> &raquo; </a>";
    var previous_head= "<a onclick='load_page("+ page_head +")'  href=\"#\"> &laquo; </a>";
    var next ="<a onclick='load_page("+(page_next)+")' href=\"#\"> &rsaquo; </a>";
    var previous="<a onclick='load_page("+(page_previous)+")' href=\"#\"> &lsaquo; </a>";
    text+= previous_head + previous+ "<a onclick='load_page(" + page_head + ")' id="+page_head+" href= \"#\" >" + page_head + "</a>";  
    if(total_num_page<=Limit_handle_pages){
        for (var i = page_start; i <= total_num_page-1; i++) {
            text += "<a  onclick='load_page(" + i + ")' id="+i+" href=\"#\">" + i + "</a>";
        }
    }
    if(total_num_page>Limit_handle_pages){
            if(current_page>=limit_current_page_handle && current_page<= total_num_page-limit_current_page_handle){
                
                text += "<a  onclick='load_page(" + (current_page-Limit_hidden_pages) + ")' id="+(current_page-Limit_hidden_pages)+" href=\"#\">...</a>";
                text += "<a  onclick='load_page(" + (page_previous) + ")' id="+(current_page-Limit_show_pages)+" href=\"#\">" + (current_page-1) + "</a>";
                text += "<a  onclick='load_page(" + (current_page) + ")'  id="+(current_page)+" href=\"#\">" + (current_page) + "</a>";
                text += "<a  onclick='load_page(" + (page_next) + ")' id="+(current_page+Limit_show_pages)+"  href=\"#\">" + (current_page+1) + "</a>";
                text += "<a  onclick='load_page(" + (current_page+Limit_hidden_pages) + ")' id="+(current_page+Limit_hidden_pages)+"  href=\"#\">...</a>";
            }
            if( current_page<limit_current_page_handle ){
                var page_hidden=5;
                for(var i=page_start; i<=limit_current_page_handle; i++ ){
                    text += "<a onclick='load_page(" + i + ")' id="+i+" href=\"#\">"+i+"</a>";
                }
                text +="<a onclick='load_page(" + page_hidden + ")' id="+page_hidden+" href=\"#\">...</a>";
            }
            else if( current_page >(total_num_page-limit_current_page_handle)){
                var limit_page_show=3;
                text += "<a   onclick='load_page(" +( total_num_page-limit_current_page_handle )+ ")' id="+( total_num_page-limit_current_page_handle )+" href=\"#\">...</a>";
                for(var i=(total_num_page-limit_page_show);i<total_num_page;i++){
                    text += "<a  onclick='load_page(" + i + ")' id="+i+" href=\"#\">"+i+"</a>";
                }
            }   
    }
    text += "<a  onclick='load_page(" + page_end + ")' id="+( page_end )+" href=\"#\">" + page_end + "</a>" +next + next_end; 
    document.getElementsByClassName("pagination")[0].innerHTML = text ;
}
function show_info_table(obj) {
    var text = "";
    for (var i = 0; i < obj.length; i++) {
        text += "<tr>"
            + "<td>" + obj[i].stt + "</td>"
            + " <td>" + obj[i]["province/city"] + "</td>"
            + "<td>" + obj[i].District + "</td>"
            + "<td>" + obj[i].Commune 
            + "<i class=\"fas fa-map-marked-alt icon-location\"></i>"
            + "</td>"
            + "<td>"
            + obj[i].Species
            + "</td>"
            + "<td>" + obj[i].Volume + "</td>"
            + "<td>" + obj[i].Area + "</td>"
            + "<td>"
            + "<button class=\"btn btn-primary btn-sm btn-circle\">"
            + "<i class=\"far fa-eye\"></i>"
            + "chi tiết"
            + "</button>"
            + "</td>"
            + "</tr>";
    }
    document.getElementById("table-body").innerHTML = text;
}
function show_info_page(element_start,element_table_size,total_element){   
    var element_end = element_start + element_table_size;
    var text = "";
    text += " Hiển thị "+element_start+" - "+element_end+" của "+total_element;
    text += " <small>(show "+element_start+" - "+element_end+" of "+total_element+")</small>";
    document.getElementsByClassName("info-element-in-table")[0].innerHTML=text;
}
function active(current_page){
    document.getElementById(current_page).classList.add("active");
}

show_info_table(element_default);
show_info_page(element_page_start_default,element_page_size_default,obj_info_table.length);
show_info_pagination(total_num_page,current_page_default);
active(element_page_start_default);





